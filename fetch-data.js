var DEBUG = true;

var fs = require('fs');

var casper = require('casper').create({
    verbose: false,
    logLevel: "info",
    viewportSize: {
        width: 1280,
        height: 960
    },
    pageSettings: {
        // always use the most common browser and OS here!
        userAgent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:23.0) Gecko/20130404 Firefox/23.0",
        loadImages: true, // set to false to not load images
        loadPlugins: false // set to false to not load NPAPI plugins (Flash, Silverlight, ...)
    }
});

/*******************************************************************
 * Disable selected resources
 * (load page faster and consumes less bandwidth)
 *******************************************************************/
/*
casper.options.onResourceRequested = function(casper, requestData, request) {
    // If any of these strings are found in the requested resource's URL, skip
    // this request. These are not required for running tests.
    var skip = [
        'www.adsensecustomsearchads.com',
        'googleads.g.doubleclick.net',
        'cm.g.doubleclick.net',
        'www.googleadservices.com',
        'facebook.com',
        'connect.facebook.net',
        'www.facebook.com',
        'doubleclick.net',
        'cl.qualaroo.com',
        'maps.googleapis.com',
        'cdnssl.clicktale.net',
        'gateway.foresee.com',
        'consent.trustarc.com',
        'c.go-mpulse.net',
        'ssl.google-analytics.com',
        'www.googletagmanager.com'
    ];

    skip.forEach(function(needle) {
        if (requestData.url.indexOf(needle) > 0) {
            request.abort();
        }
    })
};
*/

// in some cases we might need to simulate the mouse click, especially when
// the site uses ReactJS
var mouse = require("mouse").create(casper);

/*******************************************************************
 * Parameters and initialization
 * we fetches a couple of values which are useful to understand 
 * in which UID we are (to push new URL to the correct task!)
 * and other information like the OUTPUT directory, where we store
 * images and other data
 *******************************************************************/

// CLI parameters parsing
var url = casper.cli.get(0);
casper.echo(">>>>> Fetching " + url);

var urlName = casper.cli.get(1);
casper.echo(">>>>> Known as " + urlName);

var uid = String(casper.cli.get(2));
casper.echo(">>>>> UID is " + uid);

var output = casper.cli.get(3) + '/' + uid;
casper.echo(">>>>> Output folder is " + output);

var taskName = casper.cli.get(4);
casper.echo(">>>>> task Name is " + taskName);

var scrapngoServer = casper.cli.get(5);
if (scrapngoServer == undefined) {
    // by default we are assuming that the reverse-SSH connection is available
    scrapngoServer = "http://localhost:8080";
}
casper.echo(">>>>> scrapngo server is  " + scrapngoServer);

/*******************************************************************
 * EXTRACT parameters from URL
 * this is a simple workaround to pass custom data between two
 * scraping scripts.
 * We are using below format, which can of course changed.
 * 
 * https://www.codref.com/{{"inputLocation": "here", "retried": o}}'
 * 
 *******************************************************************/
var extraParams = {};
try {
    var extraParamsString = url.match(/.*\{(\{.*\})\}/)[1];
    console.log('Extra params JSON string is: ' + extraParamsString);
    extraParams = JSON.parse(extraParamsString);
    url = url.replace(/\{\{.*\}\}/, "");
    console.log('Request URL is now: ' + url);
} catch (err) {}

var retried = extraParams.retried;
casper.echo(">>>>> retried attempt is " + retried);


// print out all the messages in the headless browser context
casper.on('remote.message', function(msg) {
    this.echo('remote message caught: ' + msg);
});

// print out all the messages in the headless browser context
casper.on("page.error", function(msg, trace) {
    this.echo("Page Error: " + msg, "ERROR");
});

/*******************************************************************
 * OUTPUT files definition and project-wide variables
 * these can be changed, although it is a good idea to stick to
 * common format among different projects
 *******************************************************************/
var currentTime = new Date();
var month = currentTime.getMonth() + 1;
var day = currentTime.getDate();
var year = currentTime.getFullYear();
var outputFile = output + "/" + urlName + "-" + year + "-" + month + "-" + day + ".json";
var errorFile = output + "/errors-" + year + "-" + month + "-" + day + ".json";
var outputImage = output + "/" + urlName + "-" + year + "-" + month + "-" + day + ".png";

// Meta variable decalaration
// are usually passed to the page.evaluate context
var meta = {
    "run_id": "000",
    "screenshot_id": outputImage,
};


/*******************************************************************
 * ENQUEUE URL function
 * it should be used within the browser context, internally to a
 * page.evaluate
 * 
 * It requires:
 * taskname: string; the name of the task, passed back to scrapngo server
 * ui: string; the UID of the running task, passwd back to the scrapngo server
 * urlName: string; can be manually specified, if consistent with the one defined
 *          in the task.yml file, can bypass the regex
 * urls: array of strings; the URLs to be pushed to scrapngo server
 *       may or may not include the above extraParams formatted as JSON string 
 * fast: bool; if true will use the "fast" queue rather than the strandard one
 *       fast queue is processed before the standard and is usually less crowded.
 *       Moreover the queue mechanisms is FIFO (rather than FILO)
 * infiniteRetry: bool; if true will bypass the "url duplication check"
 * scrapngoServer: string; is the URL of the scrapnngo server for the enqueue
 *******************************************************************/
function enqueueURL(taskName, uid, urlName, urls, fast, infiniteRetry, scrapngoServer) {
    var postData = [];
    for (var i = 0; i < urls.length; i++) {
        postData.push({
            url: urls[i],
            command: '',
            uid: uid,
            fast: fast,
            infiniteRetry: infiniteRetry
        })
    }

    var result;
    try {
        result = __utils__.sendAJAX(scrapngoServer + '/enqueue/' + taskName + '/' + urlName, 'POST', JSON.stringify(postData), false, { "Connection": "close" });
        if (JSON.parse(result) != 'ok') {
            console.log('Cannot send URL to server: ' + url);
        }
    } catch (err) {
        console.log('Error, cannot send URL to server: ' + err + " - " + scrapngoServer);
    }
}

/*******************************************************************
 * WRITE TO FILE function
 * Support function to serialize scraped data and save it to file
 *******************************************************************/

var writeToFile = function(data, outputFile) {
    if (data != undefined && data.length > 0) {
        var jsonData = '';
        for (var i = 0, row; row = data[i]; i++) {
            jsonData += JSON.stringify(row) + "\n";
        }
        fs.write(outputFile, jsonData, 'a');
    }
}


/*******************************************************************
 * RETRY function
 * Provides an example of "retry" logic.
 * It passes, via extraParams, a "retried" value, which is incremented
 * at every retry.
 * If the value is less than 4, the page is usually not available
 * or the scraping logic needs to be adjusted (i.e. higher latency
 * and timeouts are triggered)
 *******************************************************************/

function retry(me) {
    if (extraParams["retried"] < 4) {
        extraParams["retried"] = extraParams["retried"] + 1;

        // we need to be inside the browser to enqueue new URL!
        me.evaluate(function(extraParams, taskName, uid, urlName, enqueueFunction, scrapngoServer) {
            enqueueFunction(taskName, uid, urlName, [
                'https://www.codref.com/{' + JSON.stringify(extraParams) + '}'
            ], false, false, scrapngoServer);
        }, { extraParams: extraParams, taskName: taskName, uid: uid, urlName: urlName, enqueueFunction: enqueueURL, scrapngoServer: scrapngoServer });
    } else {
        writeToFile([{ url: url, extraParams: extraParams }], errorFile);
    }
}

/*******************************************************************
 * GET JOBS function - scraping data routine
 * Usually is a common thing to confine all the data scraping logic within
 * a single function. 
 * This code runs internally to a page.evaluate function, therefore consider
 * that any error in here is difficult to debug.
 * ALWAYS include specific try-catch to not exit the loop prematurely!
 * 
 * The function returns an array to the casperJS main process
 * which is then serialized to a format like CSV or JSON and saved to disk
 *******************************************************************/

function getJobs(meta, taskName, uid, urlName, enqueueFunction) {
    var jobs = [];
    try {
        // we are in the page we want to scrape, start fetching data
        console.log('started scraping results for url ' + document.location);
        var text, rows = document.querySelectorAll('ul > li');
        for (var i = 0, row; row = rows[i]; i++) {
            var job = {};
            job['run_id'] = taskName + "-" + uid;
            job['url'] = document.location.href;
            job['screenshot_id'] = meta['screenshot_id'];
            job['text'] = row.innerText;
            jobs.push(job);
        }

        // we can also start enqueing new URLs in here (or in above loop)!
        // everything is left to the developer which might want to
        // split the code logic into several micro-steps

    } catch (err) {
        jobs.push({ 'error': err.message, 'rowId': 'somthing useful here' });
    }

    return jobs;
}

/*******************************************************************
 * Actual fetching logic
 * This example uses a custom function processPage to group the code necessary
 * to rach the data inside the page.
 * This could involve the filling in of forms, the ckicking of buttons
 * or the URL navigation.
 * Usually, at a specific point you'll want to invoke the getJobs, which
 * usually runs inside te Browser page.
 * 
 * Timeouts here plays an important role.
 * Use them wisely. One can even define custom logic to increase them programmatically
 * (maybe multiplying the retry with a constant)
 *******************************************************************/

var processPage = function() {
    this.echo(">>>>> Initiated data fetching...");

    // we usually wait for something too here, maybe when is visible!
    this.waitUntilVisible('h1.page-header', function() {

        // we might want to save a screenshot of the page
        this.capture(outputImage);

        // but, most important, we might want to retrieve the data from inside the page
        // and store it to disk
        writeToFile(this.evaluate(getJobs, { meta: meta, taskName: taskName, uid: uid, urlName: urlName, enqueueFunction: enqueueURL }), outputFile);

        this.echo(">>>>> Terminated!");
    }, function _onTimeout() {
        // this is a timeout condition        
        this.echo('Timed out on data fetching step.');
        // we might want to re-enqueue the page
        retry();
    }, 10000);
};


/*******************************************************************
 * Web scraping START
 *******************************************************************/

casper.start(url).waitForSelector('.main-container', processPage, function() {
    // this is the timeout condition reached
    // at this point we need to retry and cross the fingers
    this.echo('Timed out on entry point condition step');

    // we might want to store locally a screenshot to let us
    // understand why things went wrong!
    this.capture(outputImage + "-error.png");

    // try changing the initial waitForSelector condition to test out the
    // retry function!
    retry(this);
}, 10000);


// last command is to run CasperJS
casper.run();