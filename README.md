# A boilerplate project to scrape data using casperJS/slimerJS and scrapngo

![How to run boilerplate project in Docker](videos/docker-test-screencast.mp4)

The project retrieve dummy data from https://codref.com website.  
The workflow simulate a two steps scraping job.  

The first step [fetch-urls.js](https://gitlab.com/scrapngo/tasks/casper-boilerplate/blob/master/fetch-urls.js) read some url from the menu and pushes them back to the **scrapngo** server (view file comments for further explanation).  

The second step [fetch-data.js](https://gitlab.com/scrapngo/tasks/casper-boilerplate/blob/master/fetch-data.js) fetches data from the codref *services* pages. In details it extracts all ```<li>``` inner text and saves a screenshot of the page.  

## Test the project via docker-compose

The project only requires below steps to be manually executed in the project folder:  

1. create ```output``` folder locally to the project folder
2. create ```.env``` file, locally to the project folder, with:

```bash
ID_RSA_PUB=[location of the id_rsa.pub file ]
ID_RSA=[location of the id_rsa file]
```

This is extremely important, as otherwise the communication between server and node will not work.  
To create this certificates on a brand new linux machine, one can use the command ```ssh-keygen```.  
There are ways to create the SSH keys also from the Docker image, although is out of scope here.

**Optional:**  
(below steps are required only in case you are using external proxies)

3. create ```credentials.txt``` file containing **in a single row, NO NEW LINES** the proxy credentials
4. create ```proxy.txt``` file containing, in each line, a valid proxy host:port

Finally, launch the docker-compose application

5. launch ```docker-compose up``` inside the project root folder


## Development via Docker

Docker (and docker-compose) can be used to make the development of a scraper easier.  
The ```docker-compose.yml``` file defines two services, the **scrapngo-node** one can also used interactively to manually test the development.  

The use is very simple. On a console type:

```bash
docker-compose run scrapngo-node bash
```

This will create a new container and will present to the user a bash console with root privileges.  
You can easily become **scrapngo** user to trigger casperJS commands manually:

```bash
su scrapngo
bash # much better console for interactive use
cd # we move to scrapngo home, which is located to /home/scrapngo
```

The scrapngo user's home has a **data** directory contains two mounted volumes:  

* the ```node-home``` folder, containing the project folder (ie. all the source code)
* the ```output``` folder, a bind-mount of the *output* directory inside the project home, the one created in the beginning.

Usually is a good habit to ship aside the scraper logic, a couple of bash scripts which can be used to test out the code.  
This boilerplate project includes two: ```test-fetch-data.sh``` and ```test-fetch-urls.sh```.  
They just issue the casperJS commands in a way similar to what scrapngo server would do.  
So usually one can issue below command:  

```bash
data/casper-boilerplate/test-fetch-urls.sh
```

or 


```bash
data/casper-boilerplate/test-fetch-data.sh
```

The output will be displayed in the console, and any generated file will be placed inside the **output** folder.  

## Test the scrapngo task

The main advantage of having a Docker stack composed of the two services (server and node) is that the whole scraper logic can be tested very quickly and inside an environment which resemble the production one.  

Right after the ```docker-compose up``` command, **scrapngo** server will be up and running waiting for REST commands.  

Use the url ```http://localhost:8080``` inside Insomnia or trigger the server operations via CURL.

For any further information, read the [API](https://gitlab.com/scrapngo/scrapngo/blob/master/docs/api.md) user guide.
