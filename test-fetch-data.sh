#!/bin/bash

casperjs --web-security=no --debug=false fetch-data.js \
    'https://codref.com/systems-administration' \
    "codref" \
    "UID" \
    "`pwd`/output" \
    "codref"
